use like
```
python ppjtest.py folder/with/tests <exact command you want to run for every test>
```

you can alias current test path variable with '%TP%' like this:
python ppjtest.py /my/tests python mycoolprogramtotest.py "<" %TP%.in ";" ... ";" # you can chain multiple commands with ;

make sure to escape special bash characters with quotation marks so that they are passed to python program as regular string parameters (redirection, file handles, pipes, logical operators etc...)
